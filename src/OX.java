import java.util.Scanner;

public class OX {
	static Scanner kb= new Scanner(System.in);
	static int r=0,c=0,i=0;
	static boolean X=false,O=false;
	static String[][] xo = {{ " ", " 1", " 2", " 3"},{"1"," -", " -", " -"},{"2"," -", " -", " -"},{"3"," -", " -", " -"}};
	static char player = 'X'; 
	static void printWelcome() {
		System.out.println("Welcome to OX Game");
	}
	static void printTable() {
		 for(int i=0;i<4;i++) {
			 for(int j=0;j<4;j++) {
				 System.out.print(xo[i][j]);
			}System.out.println();
		}
	}
	static void Input() {

		 while(true) {
			System.out.print("Please input (R,C) ");
			r=kb.nextInt();
			c=kb.nextInt();
			if(xo[r][c].equals(" -")){
				xo[r][c]=" "+player; 
				break;
			}
		 }	
		 }
	static void ShowTurn() {
		System.out.println(player+" Turn");
	}	
	static void swichPlayer() {
		if(player =='X') {
			player = 'O';
		}else {
			player = 'X';
		}
	}
	static boolean checkWin() {

		
		if(xo[1][1].equals(" X") && xo[1][2].equals(" X") && xo[1][3].equals(" X")) {
			return X=true;
		}else if(xo[2][1].equals(" X") && xo[2][2].equals(" X") && xo[2][3].equals(" X")) {
			return X=true;
		}else if(xo[3][1].equals(" X") && xo[3][2].equals(" X") && xo[3][3].equals(" X")) {
			return X=true;
		}else if(xo[1][1].equals(" O") && xo[1][2].equals(" O") && xo[1][3].equals(" O")) {
			return O=true;
		}else if(xo[2][1].equals(" O") && xo[2][2].equals(" O") && xo[2][3].equals(" O")) {
			return O=true;
		}else if(xo[3][1].equals(" O") && xo[3][2].equals(" O") && xo[3][3].equals(" O")) {
			return O=true;
		}
		
		if(xo[1][1].equals(" X") && xo[2][1].equals(" X") && xo[3][1].equals(" X")) {
			return X=true;
		}else if(xo[1][2].equals(" X") && xo[2][2].equals(" X") && xo[3][2].equals(" X")) {
			return X=true;
		}else if(xo[1][3].equals(" X") && xo[2][3].equals(" X") && xo[3][3].equals(" X")) {
			return X=true;
		}else if(xo[1][1].equals(" O") && xo[2][1].equals(" O") && xo[3][1].equals(" O")) {
			return O=true;
		}else if(xo[1][2].equals(" O") && xo[2][2].equals(" O") && xo[3][2].equals(" O")) {
			return O=true;
		}else if(xo[1][3].equals(" O") && xo[2][3].equals(" O") && xo[3][3].equals(" O")) {
			return O=true;
		}
		
		else if(xo[1][1].equals(" X") && xo[2][2].equals(" X") && xo[3][3].equals(" X")) {
			return X=true;
		}else if(xo[1][3].equals(" X") && xo[2][2].equals(" X") && xo[3][1].equals(" X")) {
			return X=true;
		}else if(xo[1][1].equals(" O") && xo[2][2].equals(" O") && xo[3][3].equals(" O")) {
			return O=true;
		}else if(xo[1][3].equals(" O") && xo[2][2].equals(" O") && xo[3][1].equals(" O")) {
			return O=true;
		}
		return false;
		 
	}
	static boolean checkDraw() {

		i++;
		if(i==9) {
			return true;
		}
		return false;
	}
	static void showWin() {
		if(X==true) {
			System.out.println("X Win!!");
		}else if(O==true) {
			System.out.println("O Win!!");
		}else if(checkDraw()) {
			showDraw();
		}
	}
	static void showDraw() {
		System.out.println("Draw !!");
		
	}
	static void showBye() {
		System.out.println("Bye Bye..");
		
	}
	public static void main(String[] args) {
		 printWelcome();
		 while(true) {
			 
			 printTable();
			 ShowTurn();
			 Input();
			 if(checkWin()) {
				break;
			 }if(checkDraw()) {
				 break;
			 }
			 swichPlayer();
		 }
		 printTable();
		 showWin();
		 showBye();
		 
		 
	}
	}